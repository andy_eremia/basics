import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

const ComponentsScreen = () => {

    const myName = "Andy";

    return (
        <View>
            <Text style={styles.textStyle}>
                This is the components screen
            </Text>
            <Text style={styles.subHeaderStyle}>
                My name is {myName}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 45
    },
    subHeaderStyle: {
        fontSize: 20
    }
});

export default ComponentsScreen;