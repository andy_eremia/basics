import React, {useState} from 'react';
import {View, StyleSheet, Text, TextInput} from 'react-native';


const TextScreen = () => {

    const [password, setPassword] = useState('');

    return (
        <View>
            <Text>Enter password</Text>
            <TextInput 
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.input}
                value={password}
                onChangeText={(newValue) => setPassword(newValue)}
            />
            {password.length < 4 ? <Text>Password must be at least 4 caracters</Text> : null}
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        margin: 15,
        borderColor: 'black',
        borderWidth: 1
    }
});

export default TextScreen;